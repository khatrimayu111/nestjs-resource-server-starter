import { Module } from '@nestjs/common';
import { NoteController } from './controllers/note/note.controller';
import { NoteService } from './entities/note/note.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Note } from './entities/note.entity';
import { CqrsModule } from '@nestjs/cqrs';
import { NoteCommandManager } from './command';
import { NoteEventManager } from './event';
import { NoteAggregatesManager } from './aggregates';
import { QueryManager } from './queries';

@Module({
  imports: [TypeOrmModule.forFeature([Note]), CqrsModule],
  providers: [
    NoteService,
    ...NoteCommandManager,
    ...NoteEventManager,
    ...NoteAggregatesManager,
    ...QueryManager,
  ],
  controllers: [NoteController],
})
export class NoteModule {}
