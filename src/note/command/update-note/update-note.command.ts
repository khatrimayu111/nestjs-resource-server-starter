import { ICommand } from '@nestjs/cqrs';
import { UpdateDto } from '../../entities/update-note.dto';

export class UpdateNoteCommand implements ICommand {
  constructor(public note: UpdateDto) {}
}
