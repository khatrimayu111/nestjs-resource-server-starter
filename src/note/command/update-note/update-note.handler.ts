import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateNoteCommand } from './update-note.command';
import { NoteAggregateService } from '../../aggregates/note-aggregate/note-aggregate.service';

@CommandHandler(UpdateNoteCommand)
export class UpdateNoteHandler implements ICommandHandler<UpdateNoteCommand> {
  constructor(
    private readonly manager: NoteAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: UpdateNoteCommand) {
    const { note: payload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.updateNote(payload);
    aggregate.commit();
  }
}
