import { ICommand } from '@nestjs/cqrs';
import { NoteDto } from '../../entities/note-dto';

export class DeleteNoteCommand implements ICommand {
  constructor(public uuid: NoteDto) {}
}
