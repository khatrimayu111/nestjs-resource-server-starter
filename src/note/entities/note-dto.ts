import { IsNotEmpty, IsString, IsOptional } from 'class-validator';

export class NoteDto {
  @IsOptional()
  uuid: string;

  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsString()
  message: string;
}
