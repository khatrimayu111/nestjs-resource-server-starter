import { BaseEntity, Entity, ObjectID, ObjectIdColumn, Column } from 'typeorm';

@Entity()
export class Note extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  title: string;

  @Column()
  message: string;
}
