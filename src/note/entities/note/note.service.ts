import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Note } from '../note.entity';
import { MongoRepository } from 'typeorm';

@Injectable()
export class NoteService {
  constructor(
    @InjectRepository(Note)
    private readonly noteRepository: MongoRepository<Note>,
  ) {}

  async find() {
    return await this.noteRepository.find();
  }

  async findOne(param) {
    return await this.noteRepository.findOne({ uuid: param });
  }

  async deleteOne(param) {
    return await this.noteRepository.deleteOne({ uuid: param });
  }
  async create(note) {
    const provider = new Note();
    Object.assign(provider, note);
    return await provider.save();
  }

  async update(incomingUuid, note) {
    const provider = await this.noteRepository.findOne({ uuid: incomingUuid });
    if (!provider) {
      throw new BadRequestException('Note not Found!!!');
    }
    Object.assign(provider, note);
    return provider.save();
  }
}
