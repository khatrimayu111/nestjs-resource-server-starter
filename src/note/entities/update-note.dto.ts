import { IsNotEmpty, IsOptional } from 'class-validator';

export class UpdateDto {
  @IsNotEmpty()
  uuid: string;

  @IsOptional()
  title: string;

  @IsOptional()
  message: string;
}
