import { Test, TestingModule } from '@nestjs/testing';
import { NoteController } from './note.controller';
import { NoteService } from '../../entities/note/note.service';
import { CqrsModule } from '@nestjs/cqrs';

describe('Note Controller', () => {
  let controller: NoteController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NoteController],
      imports: [CqrsModule],
      providers: [
        {
          provide: NoteService,
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<NoteController>(NoteController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
