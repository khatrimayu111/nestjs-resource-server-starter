import {
  Controller,
  Get,
  Param,
  Body,
  Post,
  UsePipes,
  ValidationPipe,
  Delete,
} from '@nestjs/common';
import { NoteService } from '../../entities/note/note.service';
import { NoteDto } from '../../entities/note-dto';
import * as uuidv4 from 'uuid/v4';
import { UpdateDto } from '../../entities/update-note.dto';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateNoteCommand } from '../../command/create-note/create-note.command';
import { UpdateNoteCommand } from '../../command/update-note/update-note.command';
import { DeleteNoteCommand } from '../../command/delete-note/delete-note.command';
import { ListNoteQuery } from '../../queries/list-note/list-note.query';

@Controller('note')
export class NoteController {
  constructor(
    private noteService: NoteService,
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Get('readall')
  findAll() {
    // return this.noteService.find();
    return this.queryBus.execute(new ListNoteQuery());
  }

  @Get('readone/:uuid')
  findOne(@Param('uuid') uuid) {
    return this.noteService.findOne(uuid);
  }

  @Post('create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() payload: NoteDto) {
    payload.uuid = uuidv4();
    return this.commandBus.execute(new CreateNoteCommand(payload));
    // return this.noteService.create(payload);
  }

  @Post('update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  update(@Body() payload: UpdateDto) {
    return this.commandBus.execute(new UpdateNoteCommand(payload));
    // return this.noteService.update(payload.uuid, payload);
  }

  @Delete('delete/:uuid')
  delete(@Param('uuid') uuid) {
    return this.commandBus.execute(new DeleteNoteCommand(uuid));
    // return this.noteService.deleteOne(uuid);
  }
}
