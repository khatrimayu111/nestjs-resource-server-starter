import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { NoteDto } from '../../entities/note-dto';
import { NoteCreatedEvent } from '../../event/note-created/note-created.event';
import { UpdateDto } from '../../entities/update-note.dto';
import { NoteUpdatedEvent } from '../../event/note-updated/note-updated.event';
import { NoteDeletedEvent } from '../../event/note-deleted/note-deleted.event';
import { NoteService } from '../../entities/note/note.service';

@Injectable()
export class NoteAggregateService extends AggregateRoot {
  constructor(private readonly noteService: NoteService) {
    super();
  }

  createNote(note: NoteDto) {
    this.apply(new NoteCreatedEvent(note));
  }

  updateNote(note: UpdateDto) {
    this.apply(new NoteUpdatedEvent(note));
  }

  deleteNote(uuid: NoteDto) {
    this.apply(new NoteDeletedEvent(uuid));
  }

  list() {
    return this.noteService.find();
  }
}
