import { Test, TestingModule } from '@nestjs/testing';
import { NoteAggregateService } from './note-aggregate.service';
import { NoteService } from '../../entities/note/note.service';
describe('commentsAggregateService', () => {
  let service: NoteAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NoteAggregateService,
        {
          provide: NoteService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<NoteAggregateService>(NoteAggregateService);
  });
  NoteAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
