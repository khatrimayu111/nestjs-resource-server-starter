import { NoteCreatedHandler } from './note-created/note-created.handler';
import { NoteUpdatedHandler } from './note-updated/note-updated.handler';
import { NoteDeletedHandler } from './note-deleted/note-deleted.handler';

export const NoteEventManager = [
  NoteCreatedHandler,
  NoteUpdatedHandler,
  NoteDeletedHandler,
];
