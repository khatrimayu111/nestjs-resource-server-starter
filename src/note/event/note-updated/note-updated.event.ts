import { IEvent } from '@nestjs/cqrs';
import { UpdateDto } from '../../entities/update-note.dto';

export class NoteUpdatedEvent implements IEvent {
  constructor(public note: UpdateDto) {}
}
