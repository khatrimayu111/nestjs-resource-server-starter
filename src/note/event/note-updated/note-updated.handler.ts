import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { NoteUpdatedEvent } from './note-updated.event';
import { NoteService } from '../../entities/note/note.service';

@EventsHandler(NoteUpdatedEvent)
export class NoteUpdatedHandler implements IEventHandler<NoteUpdatedEvent> {
  constructor(private readonly noteservice: NoteService) {}

  async handle(event: NoteUpdatedEvent) {
    const { note: payload } = event;
    await this.noteservice.update(payload.uuid, payload);
  }
}
