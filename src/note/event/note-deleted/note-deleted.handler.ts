import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { NoteDeletedEvent } from './note-deleted.event';
import { NoteService } from '../../entities/note/note.service';

@EventsHandler(NoteDeletedEvent)
export class NoteDeletedHandler implements IEventHandler<NoteDeletedEvent> {
  constructor(private readonly noteService: NoteService) {}

  async handle(event: NoteDeletedEvent) {
    const { uuid } = event;
    await this.noteService.deleteOne(uuid);
  }
}
