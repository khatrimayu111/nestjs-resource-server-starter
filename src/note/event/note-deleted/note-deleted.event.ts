import { IEvent } from '@nestjs/cqrs';
import { NoteDto } from '../../entities/note-dto';

export class NoteDeletedEvent implements IEvent {
  constructor(public uuid: NoteDto) {}
}
